Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Root route #
  get '/', to: 'sbif#index'

  # Sbif routes #
  get 'sbif', to: 'sbif#index'
  get 'sbif/dolar', to: 'sbif#dolar_in_range'
  get 'sbif/tmc', to: 'sbif#tmc_in_period'
  get 'sbif/uf', to: 'sbif#uf_in_range'

  # Error routes #

  # 404 #
  get '*unmatched_route', to: 'errors#not_found'
end
