# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ----- PARAMETERS ------- #

# Delete existing records
Parameter.delete_all

# Reset autoincrements
case ActiveRecord::Base.connection.adapter_name
when 'PostgreSQL'
    ActiveRecord::Base.connection.reset_pk_sequence!('parameters')
when 'Mysql2'
    update_seq_sql = "ALTER TABLE parameters AUTO_INCREMENT = 1;"
    ActiveRecord::Base.connection.execute(update_seq_sql)
end

# Seed records
Parameter.create(
    key: 'api-sbif-key',
    value: '1048ff405b6787171eeedb555a96663b0d00ca71'
)
Parameter.create(
    key: 'api-sbif-base-url',
    value: 'http://api.sbif.cl/api-sbifv3/recursos_api/'
)
