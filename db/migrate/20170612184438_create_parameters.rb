class CreateParameters < ActiveRecord::Migration[5.1]
  def change
    create_table :parameters do |t|
      t.string :key
      t.string :value    
    end
    add_index :parameters, :key, unique: true
  end
end
