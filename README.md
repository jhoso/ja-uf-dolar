# README

This application uses:

- Ruby 2.1.3 & Rails 5.1.1 on API implementation
- React 15.5.4 on Frontend implementation

It can work fine with databases:
 - MySQL Database (tested on 5.7.18)
 - PostgreSQL (tested on 9.8) 

To create the database is enough with run
- rake db:create
- rake db:migrate
- rake db:seed

To start the app you have to run

rake start