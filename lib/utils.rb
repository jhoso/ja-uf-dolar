module Utils
  def string_to_decimal(string)
    string.gsub('.', '').gsub(',', '.').to_d
  end

  def decimal_to_string(double)
    ActiveSupport::NumberHelper.number_to_rounded(double, delimiter: '.', separator: ',', precision: 2)
  end

  def zero
    "0,00"
  end

  def infinity
    "Infinity"
  end
end