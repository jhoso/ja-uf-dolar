import React, { Component } from 'react';
import CustomLine from './charts/line';

// Simplify window syntax
const $ = window.$

class SbifTmc extends Component {
  // Listeners
  getTmcInRange(e) {
    // Fech rails API to get data
    var url = "sbif/tmc?" + $.param({init_date: $('#tmcInitDate').val(), end_date: $('#tmcEndDate').val()})
    fetch(url)
      .then(response => response.json())
      .then(json => {
        // Print output
        console.log(json)
        /*var ctx = $('#tmcLineChart')
        var lineChart = new CustomLine(ctx)
        console.log(lineChart.update())*/
      }).catch(error => {
        alert(error)
      }
    )
  }

  render(){
    return (
      <div id="usd-container">
        <h2>Valor de tasa TMC por tipos para un periodo</h2>
        <form className="form-inline">
          <div className="form-group">
            <div className="input-group">
              <div className="input-group-addon">
                <span className="glyphicon glyphicon-calendar"></span>
              </div>
              <input type="text" className="form-control" id="tmcInitDate" placeholder="Init date" required="true" />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <div className="input-group-addon">
                <span className="glyphicon glyphicon-calendar"></span>
              </div>
              <input type="text" className="form-control" id="tmcEndDate" placeholder="End date" required="true" />
            </div>
          </div>
          <button type="button" className="btn btn-primary spacing" onClick={this.getTmcInRange}>Go!</button>
        </form>
        <CustomLine id="tmcLineChart" />
      </div>
    );
  }
}

export default SbifTmc;