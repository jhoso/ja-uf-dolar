import React, { Component } from 'react';
import CustomLine from './charts/line';

// Simplify window syntax
const $ = window.$

class SbifDolar extends Component {
  // Listeners
  getUsdInRange(e) {
    // Fech rails API to get data
    var url = "sbif/dolar?" + $.param({init_date: $('#usdInitDate').val(), end_date: $('#usdEndDate').val()})
    fetch(url)
      .then(response => response.json())
      .then(json => {
        // Set max
        $('#usd-max-value').val("Max: " + json.max)
        // Set min
        $('#usd-min-value').val("Min: " + json.min)
        // Set avg
        $('#usd-avg-value').val("Avg: " + json.avg)
        /*var ctx = $('#usdLineChart')
        var lineChart = new CustomLine(ctx)
        console.log(lineChart.update())*/
      }).catch(error => {
        alert(error)
      }
    )
  }

  render(){
    return (
      <div id="usd-container">
        <h2>Valor del Dolar para un rango de fechas</h2>
        <form className="form-inline">
          <div className="form-group">
            <div className="input-group">
              <div className="input-group-addon">
                <span className="glyphicon glyphicon-calendar"></span>
              </div>
              <input type="text" className="form-control" id="usdInitDate" placeholder="Init date" required="true" />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <div className="input-group-addon">
                <span className="glyphicon glyphicon-calendar"></span>
              </div>
              <input type="text" className="form-control" id="usdEndDate" placeholder="End date" required="true" />
            </div>
          </div>
          <button type="button" className="btn btn-primary spacing" onClick={this.getUsdInRange}>Go!</button>
        </form>
        <CustomLine id="usdLineChart" />
      </div>
    );
  }
}

export default SbifDolar;