import React from 'react';
import ReactDOM from 'react-dom';
import SbifDolar from './SbifDolar';
import SbifTmc from './SbifTmc';
import SbifUf from './SbifUf';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

ReactDOM.render(<SbifDolar />, document.getElementById('usd-data'));
ReactDOM.render(<SbifTmc />, document.getElementById('tmc-data'));
ReactDOM.render(<SbifUf />, document.getElementById('uf-data'));
registerServiceWorker();
