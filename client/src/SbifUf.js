import React, { Component } from 'react';
import CustomLine from './charts/line';

// Simplify window syntax
const $ = window.$

class SbifUf extends Component {
  // Listeners
  getUfInRange(e) {
    // Fech rails API to get data
    var url = "sbif/uf?" + $.param({init_date: $('#ufInitDate').val(), end_date: $('#ufEndDate').val()})
    fetch(url)
      .then(response => response.json())
      .then(json => {
        // Set max
        $('#uf-max-value').val("Max: " + json.max)
        // Set min
        $('#uf-min-value').val("Min: " + json.min)
        // Set avg
        $('#uf-avg-value').val("Avg: " + json.avg)
        /*var ctx = $('#ufLineChart')
        var lineChart = new CustomLine(ctx)
        console.log(lineChart.update())*/
      }).catch(error => {
        alert(error)
      }
    )
  }

  render(){
    return (
      <div id="uf-container">
        <h2>Valor de la UF para un rango de fechas</h2>
        <form className="form-inline">
          <div className="form-group">
            <div className="input-group">
              <div className="input-group-addon">
                <span className="glyphicon glyphicon-calendar"></span>
              </div>
              <input type="text" className="form-control" id="ufInitDate" placeholder="Init date" required="true" />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <div className="input-group-addon">
                <span className="glyphicon glyphicon-calendar"></span>
              </div>
              <input type="text" className="form-control" id="ufEndDate" placeholder="End date" required="true" />
            </div>
          </div>
          <button type="button" className="btn btn-primary spacing" onClick={this.getUfInRange}>Go!</button>
        </form>
        <CustomLine id="ufLineChart" ref="chart" />
      </div>
    );
  }
}

export default SbifUf;