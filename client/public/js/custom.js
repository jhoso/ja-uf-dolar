$(function () {
  // UF datepickers
  $('#ufInitDate').datetimepicker({
    format: 'YYYY-MM-DD',
    maxDate: 'now'
  });
  $('#ufEndDate').datetimepicker({
    format: 'YYYY-MM-DD',
    maxDate: 'now'
  });
  $("#ufInitDate").on("dp.change", function (e) {
      $('#ufEndDate').data("DateTimePicker").minDate(e.date);
  });
  $("#ufEndDate").on("dp.change", function (e) {
      $('#ufInitDate').data("DateTimePicker").maxDate(e.date);
  });

  // Dolar datepickers
  $('#usdInitDate').datetimepicker({
    format: 'YYYY-MM-DD',
    maxDate: 'now'
  });
  $('#usdEndDate').datetimepicker({
    format: 'YYYY-MM-DD',
    maxDate: 'now'
  });
  $("#usdInitDate").on("dp.change", function (e) {
      $('#usdEndDate').data("DateTimePicker").minDate(e.date);
  });
  $("#usdEndDate").on("dp.change", function (e) {
      $('#usdInitDate').data("DateTimePicker").maxDate(e.date);
  });

  // TMC datepickers
  $('#tmcInitDate').datetimepicker({
    format: 'YYYY-MM-DD',
    maxDate: 'now'
  });
  $('#tmcEndDate').datetimepicker({
    format: 'YYYY-MM-DD',
    maxDate: 'now'
  });
  $("#tmcInitDate").on("dp.change", function (e) {
      $('#tmcEndDate').data("DateTimePicker").minDate(e.date);
  });
  $("#tmcEndDate").on("dp.change", function (e) {
      $('#tmcInitDate').data("DateTimePicker").maxDate(e.date);
  });
});