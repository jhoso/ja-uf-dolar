class ErrorsController < ApplicationController
  def not_found
    error = { message: 'Routing error, content not found'}.to_json
    render json: error, status: :not_found
  end
end
