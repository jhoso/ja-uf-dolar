class SbifController < ApplicationController
  # Include modules
  include Utils

  # Declare before action
  before_action :prepare_data, except: [:index]

  # Constants
  REQUEST_FORMAT = 'json' 
  
  # Default action
  def index
    render json: {message: '¡Welcome!'}.to_json, status: :ok
  end

  def dolar_in_range
    # Prepare api url
    url = "#{@api_url}dolar/posteriores/#{(@init_date - 1.year).year}"
    get_and_process(url, "Dolares") and return
  end

  def tmc_in_period
    url = "#{@api_url}tmc/periodo/#{sprintf("%04d/%02d",@init_date.year, @init_date.month)}/#{sprintf("%04d/%02d",@end_date.year, @end_date.month)}"
    get_and_process_by_type(url, "TMCs") and return
  end

  def uf_in_range
    # Prepare api url
    url = "#{@api_url}uf/posteriores/#{(@init_date - 1.year).year}"
    get_and_process(url, "UFs") and return
  end

  # Private methods
  private
  def prepare_data
    # Permit and parse parameters
    params.permit(:init_date, :end_date)
    begin
      @init_date = Date.parse(params[:init_date])
      @end_date = Date.parse(params[:end_date])
    rescue => e
      render_error('Error parsing dates', :bad_request) and return
    end

    # Check init date less than end date
    if @init_date > @end_date
      render_error('Invalid date range', :bad_request) and return
    end
    
    # Get API information from database
    @apikey = Parameter.find_by(key: Parameter::API_KEY).value
    @api_url = Parameter.find_by(key: Parameter::API_BASE_URL).value
  end

  def get_and_process(url, data_index)
    # Call SBIF API
    begin
      response = RestClient.get(url, params: {
        apikey: @apikey,
        formato: REQUEST_FORMAT
      })
    rescue => e
      # Render error
      render_error(e.message, e.response.code) and return
    end

    # Get and process response data
    data = JSON.parse(response.body)
    processed = {}
    date_range = (@init_date..@end_date).to_a
    # Initialize max and min
    max = zero
    min = infinity
    # Initialize average
    avg_sum = 0
    # From data, select only results between init and end dates
    data[data_index].each do |d|
      if date_range.include? Date.parse(d["Fecha"])
        # Fill values array
        valor = d["Valor"]
        processed[d["Fecha"]] = valor
        # Update max
        max = valor if valor > max 
        # Update min
        min = valor if valor < min
        # Update avg
        avg_sum += string_to_decimal(valor)
      end
    end
    # Calculate avg
    avg = decimal_to_string((avg_sum/processed.size))

    # Render result
    render json: {
      message: 'Request completed',
      avg: avg,
      max: max,
      min: min,
      values: processed
    }.to_json, status: :ok
  end

  def get_and_process_by_type(url, data_index)
    # Call SBIF API
    begin
      response = RestClient.get(url, params: {
        apikey: @apikey,
        formato: REQUEST_FORMAT
      })
    rescue => e
      # Render error
      render_error(e.message, e.response.code) and return
    end

    # Get and process response data
    data = JSON.parse(response.body)
    processed = {}
    data[data_index].each do |d|
      # Check if type exists
      if processed[d["Tipo"]]
        # Fill values array
        processed[d["Tipo"]][:values].merge!({d["Fecha"] => d["Valor"]})
      else
        processed[d["Tipo"]] = {
          title: d["Titulo"],
          subtitle: d["SubTitulo"],
          values: {d["Fecha"] => d["Valor"]}
        }
      end
    end

    # Render result
    render json: {
      message: 'Request completed',
      types: processed
    }.to_json, status: :ok
  end
end
