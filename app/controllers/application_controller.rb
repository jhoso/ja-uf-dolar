class ApplicationController < ActionController::API
  
  # Function to render general errors
  def render_error(message, code)
    render json: {message: message}.to_json, status: code
  end
end
